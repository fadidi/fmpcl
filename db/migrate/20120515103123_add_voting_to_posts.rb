class AddVotingToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :upvotes, :integer, :default => 0
    add_column :posts, :downvotes, :integer, :default => 0
    add_column :posts, :popularity, :integer, :default => 0
    add_column :posts, :rating, :integer, :default => 0

    add_index :posts, :popularity
    add_index :posts, :rating
  end
end
