class AddIpAndApprovedToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :ip, :string

    add_column :posts, :approved, :boolean, :default => false

  end
end
