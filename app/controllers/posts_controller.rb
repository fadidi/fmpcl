class PostsController < ApplicationController

  load_and_authorize_resource

  # GET /posts
  # GET /posts.json
  def index
    @posts = @posts.paginate(:page => params[:page], :per_page => 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end
 
  # GET /posts/popular
  def popular
    @posts = @posts.unscoped.order("popularity DESC").paginate(:page => params[:page], :per_page => 20)
    @message = "These are the posts which have inspired the most up- and down-voting. Love it or hate it, everyone seems to have something to say..."

    respond_to do |format|
      format.html { render 'index' }
    end
  end

  # GET /posts/rating
  def rating
    @posts = @posts.unscoped.order("rating #{params[:sort]}").paginate(:page => params[:page], :per_page => 20)
    if params[:sort] == 'DESC'
      @message = "These posts have aroused everyone's sympathy."
    else
      @message = "Wow. Take a look at the posts which have made our readers most upset."
    end

    respond_to do |format|
      format.html { render 'index' }
    end
  end

  # GET /posts/search
  def search
    @query = "%" + (params[:query] ||= '') + "%"
    @posts = Post.where("body LIKE ?", @query).paginate(:page => params[:page], :per_page => 20)
    @message = "Your search for '#{params[:query] ||= ''}' returned #{@posts.count} result#{@posts.count == 1 ? nil : 's'}."
    render 'index'
  end

  # GET /posts.json
  def data
    @posts = Post.all

    if params[:style] == 'count'
      render 'count'
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = Post.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @posts = Post.accessible_by(current_ability).paginate(:page => params[:page], :per_page => 10)
    @post = Post.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/1/edit
  def edit
    @post = Post.find(params[:id])
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(params[:post].merge(:ip => request.remote_ip))

    respond_to do |format|
      if (user_signed_in? || gotcha_valid?) && @post.save
        Pusher['posts_channel'].trigger('new_post', {
          :id => @post.id,
          :body => @post.body,
          :country => @post.country
        })
        format.html { redirect_to root_path, notice: 'Thanks for contributing!' }
        format.json { render json: @post, status: :created, location: @post }
        format.js
      else
        @posts = Post.accessible_by(current_ability).paginate(:page => params[:page], :per_page => 20)
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # POST /posts/sms
  # Moonshado SMS input
  def sms
    @post = Post.new(
      :body => params[:message],
      :ip => params[:device_address],
      :contry => 'Unknown'
    )
    if @post.save
      Pusher['posts_channel'].trigger('new_post', {
        :id => @post.id,
        :body => @post.body,
        :country => @post.country
      })
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /posts/1/upvote
  def upvote
    @post = Post.find(params[:id])
    
    respond_to do |format|
      if @post.increment!(:upvotes)
        @post.update_attributes(:popularity => @post.upvotes + @post.downvotes, :rating => @post.upvotes - @post.downvotes)
        format.js
        format.html { redirect_to new_post_path, notice: 'Your vote was cast!' }
      else
        @posts = Post.accessible_by(current_ability).paginate(:page => params[:page], :per_page => 20)
        format.js
        format.html { render action: 'new', notice: 'Your vote could not be cast.' }
      end
    end
  end

  # PUT /posts/1/downvote
  def downvote
    @post = Post.find(params[:id])
    
    respond_to do |format|
      if @post.increment!(:downvotes)
        @post.update_attributes(:popularity => @post.upvotes + @post.downvotes, :rating => @post.upvotes - @post.downvotes)
        format.js
        format.html { redirect_to new_post_path, notice: 'Your vote was cast!' }
      else
        @posts = Post.accessible_by(current_ability).paginate(:page => params[:page], :per_page => 20)
        format.js
        format.html { render action: 'new', notice: 'Your vote could not be cast.' }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to new_post_url }
      format.json { head :no_content }
    end
  end

end
