class Post < ActiveRecord::Base

  validates :body, :length => { :maximum => 255 }, :uniqueness => true, :presence => true

  default_scope :order => 'created_at DESC'
  scope :most_popular, order('popularity DESC')
  scope :highly_rated, order('rating DESC')

end
